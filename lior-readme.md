### create the containers:
1) run the command: "sudo docker-compose up"
   this commad will create 3 container: app, db & jenkins
2) to see the containers use: "sudo docker-compose ps"
### in order to see the swagger page
1) browse to: localhost:3000/apidoc
2) you can also find a print screen of the swagger page at the app root directory (the current directory)
### in order to see the jenkins initial password, do the follow:
1) run the command: "sudo docker ps -a | grep jenkins" to find the jenkins container name
2) run the command: "sudo docker exec -ti attenti-repo_jenkins_1 /bin/bash" in order to run bash command in the jenkins container
3) run the command: "cat /var/jenkins_home/secrets/initialAdminPassword" in order to get the jenkins inital password
4) the inital password is: "93a4aa557faf434eadca0af14d2b6ae0"
5) browse to: http://localhost:8080 to get to jenkins, use the inital password to login 
6) you can also find the jenkins initial passwor at the print screen located at the current directory
